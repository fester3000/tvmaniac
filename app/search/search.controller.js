'use strict';

angular.module('myApp')
    .controller('SearchController', function($scope, $http, tvLibrary) {
        var apiUrl = 'http://api.themoviedb.org/3/search/tv?query=';
        var apiKey = '&api_key=7924571078633f25078c69f3cecf48af';
        $scope.query = "";

        $scope.$watch('query', function(newValue, oldValue) {
            if(newValue !== oldValue) {
                $scope.search(newValue);
            }
        });

        $scope.search = function(query) {
            console.log('Searching');
            $http.get(apiUrl + query + apiKey).then(function(response) {
                $scope.shows = response.data.results;
            })
        }

        $scope.addToLibrary = tvLibrary.add;

        $scope.removeFromLibrary = tvLibrary.remove;

        $scope.getAllFromLibrary = tvLibrary.getAll;

        $scope.isInLibrary = tvLibrary.containsByIndex;

        $scope.search("flash");
    });
