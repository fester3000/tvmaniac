'use strict';

angular.module('myApp')
.directive('tvPoster', function() { //<---- tu mozemy wstrzykiwac zaleznosci
        return {
            restrict: 'E',
            templateUrl: 'library/tvPoster.directive.html',
            scope: {
                element: '=',
                width: '@?size'
            },
            replace: true,
            link: function(scope) {
                scope.imgSrc;
                if(!scope.width) {
                    scope.width = 300;
                }
                if(scope.element.backdrop_path) {
                    scope.imgSrc =  "http://image.tmdb.org/t/p/w" + scope.width + scope.element.backdrop_path;
                } else {
                    scope.height = parseInt(scope.width/1.775);
                    scope.imgSrc = "http://placehold.it/" + scope.width + "x" + scope.height + "?text=";
                }


            }
        }
    })