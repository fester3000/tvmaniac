'use strict';

angular.module('myApp')
    .service('tvLibrary', function ($window) {

        var shows = angular.fromJson($window.localStorage.shows) || [];

        this.add = function (show) {
            shows.push(show);
            sync();
        };

        this.getAll = function () {
            return shows.slice();
        };

        this.containsByIndex = function (id) {
            return shows.some(function (show) {
                return show.id === id;
            });
        };

        this.remove = function (id) {
            shows = shows.filter(function (show) {
                return show.id !== id;
            });
            sync();
        };


        function sync () {
            $window.localStorage.shows = angular.toJson(shows);
        }

    });