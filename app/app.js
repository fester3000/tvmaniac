'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
    'ui.router'
]).
    config(function ($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('search', {
                url: '/search',
                templateUrl: 'search/search.html',
                controller: 'SearchController'
            })
            .state('library', {
                url: '/library',
                templateUrl: 'library/library.html',
                controller: 'LibraryController'
            })
            .state('show', {
                url: '/library/:showId',
                templateUrl: 'library/show.html',
                controller: 'ShowController'
            })// <----------- angular ogarnia takie kaskady jak library.show, ale wtedy pod�laduje wszystko
            // i bedzie wymagal nowego <div ui-route>
            .state('profile', {
                url: '/profile',
                templateUrl: 'profile/profile.html'
            });

        $urlRouterProvider.otherwise('/search');
    });
